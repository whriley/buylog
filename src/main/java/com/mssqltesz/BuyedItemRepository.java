package com.mssqltesz;

import java.util.List;
import org.springframework.data.repository.CrudRepository;

import com.mssqltesz.domain.BuyedItem;

public interface BuyedItemRepository extends CrudRepository<BuyedItem, Integer> {
	List<BuyedItem> findAll();
	
	List<BuyedItem> findAllByOrderByBdateDesc();
		
}
 