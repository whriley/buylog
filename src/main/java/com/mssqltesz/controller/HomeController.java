package com.mssqltesz.controller;

import java.text.ParseException;
import java.util.Date;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import com.mssqltesz.JSONRead;
import com.mssqltesz.domain.BuyedItem;
import com.mssqltesz.domain.ForbiddenItems;
import com.mssqltesz.domain.ItemTypes;
import com.mssqltesz.service.BuyedItemService;
import com.mssqltesz.service.ForbiddenItemsService;
import com.mssqltesz.service.ItemTypesService;

@Controller
public class HomeController {

	private final Logger log = LoggerFactory.getLogger(this.getClass());

	private ItemTypesService itService;
	private BuyedItemService biService;
	private ForbiddenItemsService fiService;
	private String username;

	@Autowired
	public void setBiService(BuyedItemService biService) {
		this.biService = biService;
	}

	@Autowired
	public void setitService(ItemTypesService itService) {
		this.itService = itService;
	}
	
	@Autowired
	public void setitService(ForbiddenItemsService fiService) {
		this.fiService = fiService;
	}
	
	public String getLoggedUsername()
	{
		Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		if (principal instanceof UserDetails) {
			username = ((UserDetails) principal).getUsername();
		} else {
			username = principal.toString();
		}
		return username;
	}

	/* Nem fontos a függvény neve */
	/* return String nev = nev.html */
	// @Secured("ROLE_USER")
	@RequestMapping("/buylog")
	public String buylog(Model model) {
		username = this.getLoggedUsername();
		model.addAttribute("Items", biService.getBuyedItemOrderByOrderByBdateDesc());
		JSONRead jreadMotivationMessage = new JSONRead("https://zenquotes.io/api/today");
		System.out.println(jreadMotivationMessage.getTodayQuote());
		model.addAttribute("TodayQuote", jreadMotivationMessage.getTodayQuote());
		model.addAttribute("forbiddenitems", fiService.getForbiddenItems());
		// LDAPConnect ld = new LDAPConnect();
		log.info("/buylog opened! [@RequestMapping] [username: " + username + "]");
		return "buylog";
	}

	// @Secured("ROLE_USER")
	@RequestMapping("/buylog/itemtypes")
	public String itemtypesView(Model model) {
		log.info("/buylog/itemtypes! [@RequestMapping]");
		model.addAttribute("ItemTypes", itService.getitemTypesByOrderByItemname());
		return "itemtypes";
	}

	// @Secured("ROLE_USER")
	@RequestMapping("/buylog/add")
	public String add(Model model) throws ParseException {
		log.info("/buylog/add opened! [@RequestMapping]");
		model.addAttribute("itTypes", itService.getitemTypesByOrderByItemname());
		model.addAttribute("ItemTypeSelected", new ItemTypes());
		BuyedItem bnew = new BuyedItem();
		bnew.setBdate(new Date());			
		model.addAttribute("BuyedItemNew", bnew);
		return "add";
	}

	// @Secured("ROLE_USER")
	@PostMapping("/buylog/add")
	public String addItem(@ModelAttribute ItemTypes ItemTypeSelected, @ModelAttribute BuyedItem BuyedItemNew) {
		username = this.getLoggedUsername();
		log.info("/buylog/add opened! [@PostMapping] [username: \"+username+\"]");
		log.info("ItemNameSelected : "+ItemTypeSelected.getItemname());
		log.info("ItemNameDesc : "+ItemTypeSelected.getDescriptiondata());
		System.out.println(BuyedItemNew.getTargetname());
		BuyedItemNew.setItemtypes(ItemTypeSelected);
		BuyedItemNew.setRecdate(new Date());
		BuyedItemNew.setRecuser(username);
		log.info(BuyedItemNew.getItemtypes().getItemname());
		biService.save(BuyedItemNew);		
		log.info("/buylog/add save: " + BuyedItemNew.toString() + "Itemtype: " + ItemTypeSelected.toString());
		return "redirect:/buylog";
	}

	@RequestMapping("/buylog/additemtype")
	public String addItemType(Model model) {
		log.info("/buylog/additemtype opened! [@RequestMapping]");
		model.addAttribute("itTypes", itService.getitemTypesByOrderByItemname()); // listázva és rendezve az itemtypes
		model.addAttribute("ItemTypeSelected", new ItemTypes());
		return "additemtype";
	}

	@PostMapping("/buylog/additemtype")
	public String additemtype(@ModelAttribute ItemTypes ItemTypeSelected) {
		username = this.getLoggedUsername();
		log.info("/buylog/additemtype opened! [@PostMapping] [username: \"+username+\"]");
		ItemTypeSelected.setRecdate(new Date());
		ItemTypeSelected.setRecuser(username);
		itService.save(ItemTypeSelected);
		log.info("/buylog/additemtype save: " + ItemTypeSelected.getItemname() + " - "
				+ ItemTypeSelected.getDescriptiondata() + " - " + ItemTypeSelected.getRecdate());
		return "redirect:/buylog/itemtypes";
	}
	

	// @Secured("ROLE_USER")
	@RequestMapping("/buylog/forbiddenitems")
	public String ReqforbiddenitemsAdd(Model model) throws ParseException {
		log.info("/buylog/forbiddenitems opened! [@RequestMapping]");
		model.addAttribute("itTypes", itService.getitemTypesByOrderByItemname());
		model.addAttribute("ItemTypeSelected", new ItemTypes());
		ForbiddenItems fitemNew = new ForbiddenItems();		
		fitemNew.setStartdate(new Date());
		model.addAttribute("forbiddenitems", fitemNew);
		return "forbiddenitems";
	}
	
	
	// @Secured("ROLE_USER")
	@PostMapping("/buylog/forbiddenitems")
	public String postForbiddenitemsAdd(@ModelAttribute ItemTypes ItemTypeSelected, @ModelAttribute ForbiddenItems fitemNew) {
		username = this.getLoggedUsername();
		log.info("/buylog/forbiddenitems opened! [@PostMapping] [username: \"+username+\"]");
		log.info(ItemTypeSelected.getItemname());
		log.info(ItemTypeSelected.getDescriptiondata());
		fitemNew.setItemtypesid(ItemTypeSelected);
		fitemNew.setDeleted(0);
		fitemNew.setRecdate(new Date());
		fitemNew.setRecuser(username);
		fiService.save(fitemNew);		
		log.info("/buylog/add save: " + fitemNew.toString() + "Itemtype: " + ItemTypeSelected.toString());
		return "redirect:/buylog";
		}
	
	@GetMapping("/buylog/buylogformupdate/{id}")
	public String buylogformupdate(@PathVariable ( value = "id") int id, Model model) 
	{
		log.info("/buylog/buylogFormUpdate/"+id+" Opened! [@GetMapping]");
		// get Buyeditem from the service
		BuyedItem bit = biService.getbuyedItem(id);
	//	ItemTypes itt = itService.getItemTypesid(bit.getItemtypes().getIt_id()); // Itt nem tiszta kell-e ez, mivel a buyeditemnek pont ezt tartalmaznia kellene
		log.info("!!!!!!!bit.ITEMTYPES :: "+bit.getItemtypes());
		log.info("!!!!!!!bit :: "+bit);
		log.info("!!!!!!!!!!!!!!!!!SelecteditemID (itt): "+bit.getItemtypes().getIt_id());
		// set Buyeditem as a model attribute to pre-populate the form
		model.addAttribute("buyeditem", bit);
		model.addAttribute("itTypes", itService.getitemTypesByOrderByItemname());
		ItemTypes itt = itService.getItemTypesid(bit.getItemtypes().getIt_id());
		model.addAttribute("ItemTypeSelected", itt);
		return "buylogformupdate";
	}
	

	// @Secured("ROLE_USER")
	@PostMapping("/buylog/buylogformupdate")
	public String buylogFormUpdate(@ModelAttribute ItemTypes ItemTypeSelected, @ModelAttribute BuyedItem buyeditem) {
	//	ItemTypeSelected = itService.getItemTypesid(ItemTypeSelected.getIt_id());
		username = this.getLoggedUsername();
		log.info("/buylog/buylogFormUpdate opened![@PostMapping] [username: \"+username+\"]");
		log.info("buyeditem.getItemtypes().getIt_id() ::::: "+buyeditem.getItemtypes().getIt_id());
		log.info("SelectedItemName (itemtypes): "+ItemTypeSelected.getItemname());
		log.info("SelectedDesc (itemtypes): "+ItemTypeSelected.getDescriptiondata());
		buyeditem.setRecdate(new Date());
		buyeditem.setRecuser(username);
		log.info("BuyedItem Updatera itt meg nincs benne a kivalasztott ItemTypes :: "+buyeditem.getItemtypes().getItemname());
		// itService.save(ItemTypeSelected);
		buyeditem.setItemtypes(ItemTypeSelected);
		biService.save(buyeditem);
		log.info("/buylog/buylogFormUpdate save: " + buyeditem.toString() + "Itemtype: " + ItemTypeSelected.toString());
		return "redirect:/buylog";
	}
	
	
	@GetMapping("/buylog/forbiddenitemsupdate/{id}")
	public String getmappingbuylogForbiddenItemsUpdate(@PathVariable ( value = "id") int id, Model model)
	{
		log.info("/buylog/forbiddenitemsupdate/"+id+" Opened! [@GetMapping]");
		// get Buyeditem from the service
		ForbiddenItems fit = fiService.getforbiddenItem(id);
		ItemTypes itt = itService.getItemTypesid(fit.getItemtypesid().getIt_id()); 
		log.info("GET : forbiddenItem.getFi_id() ::::: "+fit.getFi_id());
		model.addAttribute("fitem",fit);
		model.addAttribute("itTypes", itService.getitemTypesByOrderByItemname());
		itt = itService.getItemTypesid(fit.getItemtypesid().getIt_id());
		model.addAttribute("ItemTypeSelected", itt);
		return "forbiddenitemsupdate";
	}
	
	
	@PostMapping("/buylog/forbiddenitemsupdate")
	public String postbuylogForbiddenItemsUpdate(@ModelAttribute ItemTypes ItemTypeSelected, @ModelAttribute ForbiddenItems forbiddenItem)
	{
		username = this.getLoggedUsername();
		ItemTypeSelected = itService.getItemTypesid(ItemTypeSelected.getIt_id());
		forbiddenItem.setRecdate(new Date());
		forbiddenItem.setRecuser(username);
		forbiddenItem.setItemtypesid(ItemTypeSelected);
		log.info("/buylog/forbiddenitemsupdate opened![@PostMapping] [username: \"+username+\"]");
		log.info("forbiddenItem.getFi_id() ::::: "+forbiddenItem.getFi_id());
		log.info("forbiddenItem.getItemtypesid().getIt_id() ::::: "+forbiddenItem.getItemtypesid().getIt_id());
		log.info("SelectedItemName (itemtypes): "+ItemTypeSelected.getItemname());
		log.info("SelectedDesc (itemtypes): "+ItemTypeSelected.getDescriptiondata());
		
		fiService.save(forbiddenItem);
		log.info("/buylog/forbiddenitemsupdate/ save: " + forbiddenItem.toString() + "Itemtype: " + ItemTypeSelected.toString());
		return "redirect:/buylog";
	}
	
}
