package com.mssqltesz;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.net.URL;
import java.nio.charset.Charset;

import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class JSONRead {

	private JSONObject json;
	private final Logger log = LoggerFactory.getLogger(this.getClass());

	public JSONRead(String url) {
		try {
			json = readJsonFromUrl(url);
		} catch (JSONException e) {
			log.warn("JSON exception!" + e);
			e.printStackTrace();
		} catch (IOException e) {
			log.warn("IOException! with: " + url + " Exception: " + e);
			e.printStackTrace();
		}
	}

	private static String readAll(Reader rd) throws IOException {
		StringBuilder sb = new StringBuilder();
		int cp;
		while ((cp = rd.read()) != -1) {
			sb.append((char) cp);
		}
		return sb.toString();
	}

	public static JSONObject readJsonFromUrl(String url) throws IOException, JSONException {
		InputStream is = new URL(url).openStream();
		try {
			BufferedReader rd = new BufferedReader(new InputStreamReader(is, Charset.forName("UTF-8")));
			String jsonText = readAll(rd);
			jsonText = jsonText.substring(1, jsonText.length() - 1);
			JSONObject json = new JSONObject(jsonText);
			return json;
		} finally {
			is.close();
		}
	}

	public String getTodayQuote() {
		JSONObject jsono = this.getJson();
		String todayQuote = jsono.getString("q");
		String todayQuoteAuthor = jsono.getString("a");
		return todayQuote  +" ( " + todayQuoteAuthor + " )";
	}

	public JSONObject getJson() {
		return json;
	}

	public void setJson(JSONObject json) {
		this.json = json;
	}

}
