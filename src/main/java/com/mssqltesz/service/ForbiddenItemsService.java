package com.mssqltesz.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mssqltesz.ForbiddenItemsRepository;
import com.mssqltesz.domain.BuyedItem;
import com.mssqltesz.domain.ForbiddenItems;
import com.mssqltesz.domain.ItemTypes;

@Service
public class ForbiddenItemsService {
	
	private ForbiddenItemsRepository firepo;
	
	@Autowired
	public void setFirepo(ForbiddenItemsRepository firepo) {
		this.firepo = firepo;
	}
	
	public List<ForbiddenItems> getForbiddenItems(){
		List<ForbiddenItems> fitems = firepo.findAll();
		return fitems;
	}
	
	public void save(ForbiddenItems ForbiddenItemsadd) {
		firepo.save(ForbiddenItemsadd);
		
	}

	public ForbiddenItems getforbiddenItem(int id) {
		Optional<ForbiddenItems> optional = firepo.findById(id);
		ForbiddenItems forbiddenItems = null;
		if(optional.isPresent()) {
			forbiddenItems = optional.get();
		} else {
			throw new RuntimeException("ForbiddenItem not found for id: " + id);
		}
		return forbiddenItems;
	}
}
