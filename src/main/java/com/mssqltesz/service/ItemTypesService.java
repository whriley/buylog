package com.mssqltesz.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mssqltesz.BuyedItemRepository;
import com.mssqltesz.ItemTypesRepository;
import com.mssqltesz.domain.BuyedItem;
import com.mssqltesz.domain.ItemTypes;


@Service
public class ItemTypesService {
	
	private ItemTypesRepository itrepo;
	
	@Autowired
	public void setItrepo(ItemTypesRepository itrepo) {
		this.itrepo = itrepo;
	}
	
	
	public List<ItemTypes> getitemTypesByOrderByItemname(){
		List<ItemTypes> items = itrepo.findAllByOrderByItemname();
		return items;
	}


	public void save(ItemTypes itemTypeSelected) {
		itrepo.save(itemTypeSelected);
		
	}
	
	public ItemTypes getItemTypesid(Integer id) {
		Optional<ItemTypes> optional = itrepo.findById(id);
		ItemTypes itemtypes = null;
		if(optional.isPresent()) {
			itemtypes = optional.get();
		} else {
			throw new RuntimeException("itemtypes not found for id: " + id);
		}
		return itemtypes;
	}
	
	
}
