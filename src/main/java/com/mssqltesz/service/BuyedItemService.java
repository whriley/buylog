package com.mssqltesz.service;

import java.util.List;
import java.util.Optional;

import org.hibernate.Session;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mssqltesz.BuyedItemRepository;
import com.mssqltesz.ItemTypesRepository;
import com.mssqltesz.domain.BuyedItem;
import com.mssqltesz.domain.ItemTypes;


@Service
public class BuyedItemService {
	
	private BuyedItemRepository birepo;
	
	@Autowired
	public void setBirepo(BuyedItemRepository birepo) {
		this.birepo = birepo;
	}
	
	public List<BuyedItem> getBuyedItem(){
		List<BuyedItem> items = birepo.findAll();
		return items;
	}
	
	public List<BuyedItem> getBuyedItemOrderByOrderByBdateDesc(){
		List<BuyedItem> items = birepo.findAllByOrderByBdateDesc();
		return items;
	}

	public void save(BuyedItem buyedItemNew) {
		birepo.save(buyedItemNew);
	}
	

	public BuyedItem getbuyedItem(Integer id) {
		Optional<BuyedItem> optional = birepo.findById(id);
		BuyedItem buyeditem = null;
		if(optional.isPresent()) {
			buyeditem = optional.get();
		} else {
			throw new RuntimeException("buyeditem not found for id: " + id);
		}
		return buyeditem;
	}
	
}
