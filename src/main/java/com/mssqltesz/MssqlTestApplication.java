package com.mssqltesz;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.thymeleaf.templateresolver.ClassLoaderTemplateResolver;

@SpringBootApplication
public class MssqlTestApplication {

	public static void main(String[] args) {
		SpringApplication.run(MssqlTestApplication.class, args);
//		ClassLoaderTemplateResolver resolver = new ClassLoaderTemplateResolver();
//		resolver.setPrefix("templates");
//		resolver.setSuffix(".html");
	}

}
