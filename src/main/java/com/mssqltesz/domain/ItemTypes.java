package com.mssqltesz.domain;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.springframework.format.annotation.DateTimeFormat;

@Entity
@Table(name="itemtypes")
public class ItemTypes {
	
	@Id
	@GeneratedValue
	(strategy = GenerationType.SEQUENCE,generator = "itemtypes_generator")
	@SequenceGenerator
	(name="itemtypes_generator", sequenceName = "itemtypes_seq", 
	initialValue = 1, allocationSize = 1)
	@Column(name="it_id")
	private Integer it_id;
	private String itemname;	
	private String descriptiondata;
	@DateTimeFormat(pattern = "yyyy-MM-dd hh:mm:ss")
	private Date recdate;
	private String recuser;
	@OneToMany(mappedBy = "itemtypesid")
	private List<BuyedItem> buyeditem  = new ArrayList<>();
	
	public ItemTypes(String itemname, String descriptiondata, Date recdate, List<BuyedItem> buyeditem) {
		this.itemname = itemname;
		this.descriptiondata = descriptiondata;
		this.recdate = recdate;
		this.buyeditem = buyeditem;
	}

	public ItemTypes() {
	
	}

	public Integer getIt_id() {
		return it_id;
	}

	public void setIt_id(Integer it_id) {
		this.it_id = it_id;
	}

	public String getItemname() {
		return itemname;
	}

	public void setItemname(String itemname) {
		this.itemname = itemname;
	}

	public String getDescriptiondata() {
		return descriptiondata;
	}

	public void setDescriptiondata(String descriptiondata) {
		this.descriptiondata = descriptiondata;
	}

	public Date getRecdate() {
		return recdate;
	}

	public void setRecdate(Date recdate) {
		this.recdate = recdate;
	}


	public List<BuyedItem> getBuyeditem() {
		return buyeditem;
	}

	public void addBuyeditem(BuyedItem bi)
	{
		buyeditem.add(bi);
		bi.setItemtypes(this);
	}

	public void removeBuyeditem(BuyedItem bi)
	{
		this.buyeditem.remove(bi);
		bi.setItemtypes(null);
	}
	
	public void setBuyeditem(List<BuyedItem> buyeditem) {
		this.buyeditem = buyeditem;
	}

	
	public String getRecuser() {
		return recuser;
	}

	public void setRecuser(String recuser) {
		this.recuser = recuser;
	}

	@Override
	public String toString() {
		return "ItemTypes [it_id=" + it_id + ", itemname=" + itemname + ", descriptiondata=" + descriptiondata
				+ ", recdate=" + recdate + "]";
	}
	
	
}
