/* Új entitás (új tábla) létrehozásáhot */ 

package com.mssqltesz.domain;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.springframework.format.annotation.DateTimeFormat;

@Entity
public class ForbiddenItems {

	@Id
	@GeneratedValue
	(strategy = GenerationType.SEQUENCE,generator = "forbiddenitems_generator")
	@SequenceGenerator
	(name="forbiddenitems_generator", sequenceName = "forbiddenitems_generator", 
	initialValue = 1, allocationSize = 1)
	private Integer fi_id;
	private String forbiddenData;
	@DateTimeFormat(pattern = "yyyy-MM-dd")
	private Date startdate;
	@DateTimeFormat(pattern = "yyyy-MM-dd")
	private Date enddate;
	private String descriptiondata;
	private Date recdate;
	private String recuser;
	private Integer deleted;
	
	@ManyToOne
	private ItemTypes itemtypesid;
	 
	
	public ForbiddenItems() {
	}


	public ForbiddenItems(String forbiddenData, Date startdate, Date enddate, String descriptiondata, Date recdate,
			String recuser, Integer deleted, ItemTypes itemtypesid) {
		this.forbiddenData = forbiddenData;
		this.startdate = startdate;
		this.enddate = enddate;
		this.descriptiondata = descriptiondata;
		this.recdate = recdate;
		this.recuser = recuser;
		this.deleted = deleted;
		this.itemtypesid = itemtypesid;
	}
	
	
	public String getForbiddenData() {
		return forbiddenData;
	}
	public void setForbiddenData(String forbiddenData) {
		this.forbiddenData = forbiddenData;
	}
	public ItemTypes getItemtypesid() {
		return itemtypesid;
	}
	public void setItemtypesid(ItemTypes itemtypesid) {
		this.itemtypesid = itemtypesid;
	}
	public Integer getFi_id() {
		return fi_id;
	}
	public void setFi_id(Integer fi_id) {
		this.fi_id = fi_id;
	}
	public Date getStartdate() {
		return startdate;
	}
	public void setStartdate(Date startdate) {
		this.startdate = startdate;
	}
	public Date getEnddate() {
		return enddate;
	}
	public void setEnddate(Date enddate) {
		this.enddate = enddate;
	}
	public String getDescriptiondata() {
		return descriptiondata;
	}
	public void setDescriptiondata(String descriptiondata) {
		this.descriptiondata = descriptiondata;
	}
	public Date getRecdate() {
		return recdate;
	}
	public void setRecdate(Date recdate) {
		this.recdate = recdate;
	}
	public String getRecuser() {
		return recuser;
	}
	public void setRecuser(String recuser) {
		this.recuser = recuser;
	}
	public Integer getDeleted() {
		return deleted;
	}
	public void setDeleted(Integer deleted) {
		this.deleted = deleted;
	}


	@Override
	public String toString() {
		return "ForbiddenItems [fi_id=" + fi_id + ", forbiddenData=" + forbiddenData + ", startdate=" + startdate
				+ ", enddate=" + enddate + ", descriptiondata=" + descriptiondata + ", recdate=" + recdate
				+ ", recuser=" + recuser + ", deleted=" + deleted + ", itemtypesid=" + itemtypesid + "]";
	}
	
	
}
