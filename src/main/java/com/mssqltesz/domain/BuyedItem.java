/* Egy táblázat sorai pl. DBből => Domain */

package com.mssqltesz.domain;

import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.hibernate.annotations.CascadeType;
import org.springframework.format.annotation.DateTimeFormat;

@Entity
@Table(name="buyeditem")
public class BuyedItem {
	
	@Id
	@GeneratedValue
	(strategy = GenerationType.SEQUENCE,generator = "buyeditem_generator")
	@SequenceGenerator
	(name="buyeditem_generator", sequenceName = "buyeditem_seq", 
	initialValue = 1, allocationSize = 1)
	private Integer bi_id;
	@DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date bdate;
	private Integer quantity;
	private String targetname;
	private Integer know; 
	private String descriptiondata;
	private Date recdate;
	private String recuser;
	@ManyToOne
	private ItemTypes itemtypesid;
	
	
	public BuyedItem() {
		
	}
	
	public BuyedItem(Date bdate, Integer quantity, String targetname, Integer know, String descriptiondata,
			ItemTypes itemtypesid) {
		this.bdate = bdate;
		this.quantity = quantity;
		this.targetname = targetname;
		this.know = know;
		this.descriptiondata = descriptiondata;
		this.itemtypesid = itemtypesid;
	}


	public Integer getBi_id() {
		return bi_id;
	}


	public void setBi_id(Integer bi_id) {
		this.bi_id = bi_id;
	}


	public Date getBdate() {
		return bdate;
	}


	public void setBdate(Date bdate) {
		this.bdate = bdate;
	}


	public Integer getQuantity() {
		return quantity;
	}


	public void setQuantity(Integer quantity) {
		this.quantity = quantity;
	}


	public String getTargetname() {
		return targetname;
	}


	public void setTargetname(String targetname) {
		this.targetname = targetname;
	}


	public Integer getKnow() {
		return know;
	}


	public void setKnow(Integer know) {
		this.know = know;
	}


	public String getDescriptiondata() {
		return descriptiondata;
	}


	public void setDescriptiondata(String descriptiondata) {
		this.descriptiondata = descriptiondata;
	}


	public ItemTypes getItemtypes() {
		return itemtypesid;
	}


	public void setItemtypes(ItemTypes it_id) {
		this.itemtypesid = it_id;
	}

	
	public Date getRecdate() {
		return recdate;
	}

	public void setRecdate(Date recdate) {
		this.recdate = recdate;
	}

	
	public String getRecuser() {
		return recuser;
	}

	public void setRecuser(String recuser) {
		this.recuser = recuser;
	}

	@Override
	public String toString() {
		return "BuyedItem [bi_id=" + bi_id + ", bdate=" + bdate + ", quantity=" + quantity + ", targetname="
				+ targetname + ", know=" + know + ", descriptiondata=" + descriptiondata + ", itemtypesid="
				+ itemtypesid + "]";
	}
	
    
	
}
