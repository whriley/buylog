package com.mssqltesz;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import com.mssqltesz.domain.ForbiddenItems;

public interface ForbiddenItemsRepository extends CrudRepository<ForbiddenItems, Integer>{
		List<ForbiddenItems> findAll();
			
}

