package com.mssqltesz;

import java.util.List;
import org.springframework.data.repository.CrudRepository;

import com.mssqltesz.domain.BuyedItem;
import com.mssqltesz.domain.ItemTypes;

public interface ItemTypesRepository extends CrudRepository<ItemTypes, Integer> {
	List<ItemTypes> findAll();

	List<ItemTypes> findAllByOrderByItemname();
	
}
 