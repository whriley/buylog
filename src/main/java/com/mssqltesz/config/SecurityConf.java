package com.mssqltesz.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;

@EnableGlobalMethodSecurity(securedEnabled = true)
@Configuration
public class SecurityConf extends WebSecurityConfigurerAdapter {

	protected void configure(HttpSecurity http) throws Exception {
		http
			.authorizeRequests()
				.antMatchers("/buylog/**").hasRole("USER")
				.anyRequest().authenticated()
				.and()
			.formLogin()
	//			.loginPage("/login")
				.permitAll()
				.and()
			.logout()
	//			.logoutSuccessUrl("/login?logout")
				.permitAll();
	}	
}
