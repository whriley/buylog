package com.mssqltesz;

import java.util.ArrayList;
import java.util.Enumeration;
import java.util.Hashtable;

import javax.naming.Context;
import javax.naming.NamingEnumeration;
import javax.naming.NamingException;
import javax.naming.directory.Attribute;
import javax.naming.directory.Attributes;
import javax.naming.directory.DirContext;
import javax.naming.directory.InitialDirContext;
import javax.naming.directory.SearchControls;
import javax.naming.directory.SearchResult;
import javax.naming.ldap.InitialLdapContext;
import javax.naming.ldap.LdapContext;

public class LDAPConnect {
	   
	public LDAPConnect()
	{
		  /*
        Update these variable values before compiling.
        Note: LDAP_DN and LDAP_PW should be the same account used in the
              instance's LDAP Server record.
     */
     String LDAP_URL = "ldap://172.16.1.1";
     String LDAP_DN = "CN=Peter Richard,OU=Informatika OU,OU=ATHOS_USERS,DC=komtavhort,DC=hu";
     String LDAP_PW = "Whriley1";
     String SEARCH_BASE = "OU=ATHOS_USERS,DC=komtavhort,DC=hu";
     String FILTER = "(objectClass=user)";

     Hashtable env = new Hashtable();
     env.put(Context.INITIAL_CONTEXT_FACTORY,"com.sun.jndi.ldap.LdapCtxFactory");  //java.naming.factory.initial
     env.put(Context.PROVIDER_URL,LDAP_URL);                                       //java.naming.provider.url

     //authentication:
     env.put(Context.SECURITY_PRINCIPAL,LDAP_DN);                                  //java.naming.security.principal
     env.put(Context.SECURITY_CREDENTIALS,LDAP_PW);                                //java.naming.security.credentials
     env.put(Context.SECURITY_AUTHENTICATION,"simple");                            //java.naming.security.authentication

     try {
          DirContext ctx = new InitialDirContext(env);
          ctx = new InitialLdapContext(env, null);

          SearchControls ctrl = new SearchControls();
          ctrl.setSearchScope(SearchControls.SUBTREE_SCOPE);
          String[] returnAttrs = {"cn"};
          ctrl.setReturningAttributes(returnAttrs);

          NamingEnumeration results = ctx.search(SEARCH_BASE,FILTER,ctrl);
          int count =0;
          while( results.hasMoreElements() ) {
            count++;
            SearchResult result = (SearchResult)results.next();

            //print attributes:
            Attributes attrs = result.getAttributes();
            if(null!=attrs) {
                for(NamingEnumeration ae = attrs.getAll(); ae.hasMoreElements();) {
                  Attribute atr = (Attribute)ae.next();
                  String attrId = atr.getID();
                  for(Enumeration vals = atr.getAll();
                      vals.hasMoreElements();
                      System.out.println(attrId+": "+vals.nextElement()));
                }
             }
          } //finish iterating through all users

          System.out.println("number of items: " + count);
     } catch (NamingException e) {
          e.printStackTrace();
          System.out.println("LDAP Notifications failure. ");
     }
     
	}
		
}

